const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	firstName:{
		type: String,
		required: [true, "First Name is required."]
	},

	lastName:{
		type: String,
		required: [true, "Last Name is required."]
	},

	email:{
		type: String,
		required: [true, "E-mail is required."]
	},

	password:{
		type: String,
		required: [true, "Password is required."]
	},

	isAdmin:{
		type: String,
		default: false
	},

	mobileNo:{
		type: String,
		required: [true, "Your mobile number is required."]
	},

	enrollments: [
		{
			courseId: {
				type: String,
				required: [true, "courseId is required"]
			},
			enrolledOn: {
				type: Date,
				default: new Date ()
			},
			status: {
				type: String,
				default: [true, "You are currently enrolled."]
			}
		}
	]
});
								
module.exports = mongoose.model("Users", userSchema);