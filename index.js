const express = require("express");
const mongoose = require("mongoose");

const app = express();


app.use(cors());


app.use(express.json());
app.use(express.urlencoded({extended: true}));

mongoose.connect("mongodb+srv://admin:admin@activitys37.v5vdda6.mongodb.net/userModel?retryWrites=true&w=majority", {(
	useNewUrlParser: true,
	useUnifiedTopology: true
)});


mongoose.connection.once('open', () => console.log('Now connected to Hong-Mongo DB Atlas.'));
app.listen(process.env.PORT || 4000 () => 
	{console.log(`API is now online on port ${process.env.PORT || 4000}`)
});